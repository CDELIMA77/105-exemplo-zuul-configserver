package br.com.mastertech.pessoa.pessoa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O carro informado é inválido")
public class CarroNotFoundException extends RuntimeException {
}
