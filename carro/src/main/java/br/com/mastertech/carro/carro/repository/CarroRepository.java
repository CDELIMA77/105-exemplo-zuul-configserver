package br.com.mastertech.carro.carro.repository;

import br.com.mastertech.carro.carro.model.Carro;
import org.springframework.data.repository.CrudRepository;

public interface CarroRepository extends CrudRepository<Carro, Long> {
}
